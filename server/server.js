const mongoose = require('mongoose');

const app = require('./src/app');
const { seed } = require('./src/seed');

const port = process.env.PORT || 3005;

mongoose
  .connect('mongodb://localhost:27017/pulsarr', {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .catch((err) => {
    console.error(err);
    process.exit(-1);
  })
  .then(async () => {
    if (process.env.SEED) {
      console.log('Seeding DB');
      await seed();
    }
  })
  .then(() => {
    console.log('Seeding Done');
    app.listen(port, () => console.log(`listening on port ${port}!`));
  });
