module.exports = {
  testPathIgnorePatterns: ['/node_modules/'],
  testEnvironment: 'node', // https://mongoosejs.com/docs/jest
};
