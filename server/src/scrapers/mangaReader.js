async function getAllSeries(page) {
  await page.goto('https://www.mangareader.net/alphabetical');
  const series = await page.$$eval('.series_col li', seriesList => seriesList.map(s => ({
    title: s.querySelector('a').innerText.trim(),
    href: s.querySelector('a').href,
    completed: s.querySelector('.mangacompleted') !== null,
    source: window.location.origin,
    sourceName: 'Manga Reader',
  })));

  return series;
}

async function getAllChapters(page, seriesLink) {
  await page.goto(seriesLink);
  const chapters = await page.$$eval('#listing tr:not(:first-child)', chapterList => chapterList.map((c, index) => ({
    position: index,
    title: c.querySelector('a').innerText,
    href: c.querySelector('a').href,
    dateAddedText: c.querySelector('td:not(:first-child)').innerText,
  })));
  return chapters;
}

async function getSeriesInfo(page, seriesLink) {
  await page.goto(seriesLink);
  const seriesInfo = await page.$eval('body', b => ({
    seriesImg: b.querySelector('#mangaimg > img').src,
    dateReleased: b.querySelector(
      '#mangaproperties > table > tbody > tr:nth-child(3) > td:nth-child(2)',
    ).innerText,
    author: b.querySelector('#mangaproperties > table > tbody > tr:nth-child(5) > td:nth-child(2)')
      .innerText,
    artist: b.querySelector('#mangaproperties > table > tbody > tr:nth-child(6) > td:nth-child(2)')
      .innerText,
    description: b.querySelector('#readmangasum > p').innerText,
  }));
  return seriesInfo;
}

async function getAllPages(page, chapterLink) {
  await page.goto(chapterLink);
  const pages = await page.$$eval('#pageMenu option', chapterList => chapterList.map((p, index) => ({
    position: index,
    // eslint-disable-next-line no-undef
    href: window.location.origin + p.value,
    // TODO: likely will need to store the image locator
  })));
  return pages;
}

async function downloadImage(page, pageLink) {
  // Select the #svg img element and save the screenshot.
  await page.goto(pageLink);
  const svgImage = await page.$('#svg');
  await svgImage.screenshot({
    path: 'logo-screenshot.png',
    omitBackground: true,
  });
}

module.exports = {
  getAllSeries,
  getSeriesInfo,
  getAllChapters,
  getAllPages,
  downloadImage,
};
