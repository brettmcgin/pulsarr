const { model, Schema, ObjectId } = require('mongoose');

const Chapter = require('./Chapter');

const SeriesSchema = new Schema({
  title: {
    type: String,
    index: true,
    required: true,
  },
  href: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
  },
  dateScraped: {
    type: Date,
    default: Date.now,
    required: true,
  },
  source: {
    type: String,
  },
  sourceName: {
    type: String,
    required: true,
  },
  favorite: {
    type: Boolean,
    required: true,
    default: false,
  },
  following: {
    type: Boolean,
    required: true,
    default: false,
  },
  dateReleased: {
    type: Date,
  },
  author: {
    type: String,
  },
  artist: {
    type: String,
  },
  description: {
    type: String,
  },
  chapters: [{ type: ObjectId, ref: Chapter }],
});

module.exports = model('Series', SeriesSchema);
