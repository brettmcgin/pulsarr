const { model, Schema, ObjectId } = require('mongoose');
const Page = require('./Page');

const ChapterSchema = new Schema({
  position: {
    type: Number,
    index: true,
    required: true,
  },
  title: {
    type: String,
    index: true,
    required: true,
  },
  href: {
    type: String,
    required: true,
  },
  dateScraped: {
    type: Date,
    default: Date.now,
    required: true,
  },
  pages: [{ type: ObjectId, ref: Page }],
});

module.exports = model('Chapter', ChapterSchema);
