const mongoose = require('mongoose');

const PageSchema = new mongoose.Schema({
  position: {
    type: Number,
    index: true,
    required: true,
  },
  href: {
    type: String,
    required: true,
  },
  dateAdded: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model('Page', PageSchema);
