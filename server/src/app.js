const express = require('express');
const bodyParser = require('body-parser');
const { attachRoutes } = require('./routes/routes');

const app = express();
app.use(bodyParser.json());

attachRoutes(app);

module.exports = app;
