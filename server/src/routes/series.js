const express = require('express');
const Series = require('../model/Series');

const router = express.Router();

router.post('/', async (req, res) => {
  const { title, page, pageSize } = req.body;

  const findOpts = {};
  if (title) {
    findOpts.title = new RegExp(title, 'i');
  }

  const mangas = await Series.find(findOpts)
    .limit(pageSize || 10)
    .skip(page * pageSize || 0)
    .exec();
  res.json(mangas);
});

router.put('/', async (req, res) => {
  const { _id: id } = req.body;

  try {
    await Series.findByIdAndUpdate(id, { $set: req.body });
    res.status(200).json();
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;
