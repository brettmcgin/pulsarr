const series = require('./series');

function attachRoutes(app) {
  app.use('/series', series);
}

module.exports = {
  attachRoutes,
};
