/* eslint-disable no-await-in-loop */
const puppeteer = require('puppeteer');
const puppeteerConfig = require('./puppeteerConfig');
const Series = require('./model/Series');
const Chapter = require('./model/Chapter');
const Page = require('./model/Page');
const mangaReader = require('./scrapers/mangaReader');

async function seedManga(page) {
  const count = await Series.estimatedDocumentCount();
  if (count === 0) {
    const series = await mangaReader.getAllSeries(page);
    await Promise.all(series.map(s => new Series(s).save()));
  }
}

async function seedChapterDetail(page, series) {
  if (!series.description) {
    const chapterDetail = await mangaReader.getSeriesInfo(page, series.href);
    Object.assign(series, chapterDetail);
    await series.save();
  }
}

async function seedChapters(page, series) {
  if (series.chapters.length === 0) {
    const chaptersInfo = await mangaReader.getAllChapters(page, series.href);
    const chapterIds = [];

    for (let i = 0; i < chaptersInfo.length; i += 1) {
      const chapter = new Chapter(chaptersInfo[i]);
      console.log('chapter', chapter._id.toString());
      chapterIds.push(chapter._id);
      await chapter.save();
    }

    series.chapters = chapterIds;
    await series.save();
  }
}

async function seedPages(puppeteerPage, chapter) {
  if (chapter.pages.length === 0) {
    const pages = await mangaReader.getAllPages(puppeteerPage, chapter.href);

    const pageIds = [];

    for (let i = 0; i < pages.length; i += 1) {
      const page = new Page(pages[i]);
      console.log('page', page._id.toString());
      pageIds.push(page._id);
      await page.save();
    }

    chapter.pages = pageIds;
    await chapter.save();
  }
}

async function seedAllPages(page, series) {
  if (series.chapters.length > 0) {
    for (let i = 0; i < series.chapters.length; i += 1) {
      const chapter = await Chapter.findById(series.chapters[i]);
      await seedPages(page, chapter);
    }
  }
}

async function seed() {
  const browser = await puppeteer.launch(puppeteerConfig);
  const page = await browser.newPage();

  await seedManga(page);
  const series = await Series.findOne();
  console.log('series', series._id.toString());

  await seedChapterDetail(page, series);

  await seedChapters(page, series);
  await seedAllPages(page, series);
}

module.exports = {
  seed,
  seedManga,
};
