const puppeteer = require('puppeteer');
const puppeteerConfig = require('../../src/puppeteerConfig');
const mangaReader = require('../../src/scrapers/mangaReader');

describe('mangaReader', () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch(puppeteerConfig);
    page = await browser.newPage();
    // TODO: Move to central int test set up
    jest.setTimeout(60 * 1000);
  });

  afterAll(async () => {
    await browser.close();
  });

  let series;

  it('has more than 0 series', async () => {
    series = await mangaReader.getAllSeries(page);
    expect(series.length).toBeGreaterThan(0);

    expect(series[0].title).toBeTruthy();
    expect(series[0].href).toBeTruthy();
    // this only works because the first series is compeleted
    expect(series[0].completed).toBeTruthy();
  });

  describe('series', () => {
    let chapters;

    it('has more than 0 chapters', async () => {
      chapters = await mangaReader.getAllChapters(page, series[0].href);
      expect(chapters.length).toBeGreaterThan(0);

      expect(chapters[0].position).toBe(0);
      expect(chapters[0].title).toBeTruthy();
      expect(chapters[0].href).toBeTruthy();
      expect(chapters[0].dateAddedText).toBeTruthy();
    });

    describe('chapters', () => {
      it('has more than 0 pages', async () => {
        const pages = await mangaReader.getAllPages(page, chapters[0].href);
        expect(pages.length).toBeGreaterThan(0);

        expect(pages[0].position).toBe(0);
        expect(pages[0].href).toBeTruthy();
      });
    });
  });
});
