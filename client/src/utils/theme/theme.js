import { create } from 'jss';
import { createGenerateClassName, jssPreset, createMuiTheme } from '@material-ui/core/styles';

export const generateClassName = createGenerateClassName();

export const jss = create({
  ...jssPreset(),
});

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: "'Montserrat', 'Open Sans'",
  },
  transitions: {
    create: () => 'none',
  },
  props: {
    MuiButtonBase: {
      disableRipple: true,
    },
  },
});
