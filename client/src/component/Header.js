import React from 'react';
import { Link } from 'react-router-dom';
import { object } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Search, CloudDownload, Favorite } from '@material-ui/icons';

const styles = () => ({
  container: {
    marginTop: '3vh',
    height: '6vh',
  },
  iconContainer: {
    marginLeft: '1.5vw',
    marginRight: '1.5vw',
    height: 'inherit',
  },
  icon: {
    height: '100%',
    width: 'inherit',
  },
});

const Header = ({ classes }) => (
  <Grid className={classes.container} container justify="center">
    <Grid item className={classes.iconContainer}>
      <Link to="/">
        <Search className={classes.icon} />
      </Link>
    </Grid>
    <Grid item className={classes.iconContainer}>
      <Link to="/favorites">
        <Favorite className={classes.icon} />
      </Link>
    </Grid>
    <Grid item className={classes.iconContainer}>
      <Link to="/convert">
        <CloudDownload className={classes.icon} />
      </Link>
    </Grid>
  </Grid>
);

Header.propTypes = {
  classes: object.isRequired,
};

export default withStyles(styles)(Header);
