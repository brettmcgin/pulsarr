import React, { Component, Fragment } from 'react';
import Input from './Input';

import SeriesTable from './SeriesTable';

export default class Search extends Component {
  state = {
    searchReults: [],
  };

  handleChange = (event) => {
    this.fetchSeries(event.target.value);
  };

  handleFavorite = async (series) => {
    const updatedSeries = { ...series };
    updatedSeries.favorite = !updatedSeries.favorite;

    const { searchReults } = this.state;
    const searchReultsCopy = [...searchReults];

    const seriesIndex = searchReultsCopy.findIndex(s => s._id === updatedSeries._id);

    searchReultsCopy[seriesIndex] = updatedSeries;

    await this.updateSeries(updatedSeries);

    this.setState({ searchReults: searchReultsCopy });
  };

  fetchSeries = (title) => {
    fetch('/series', {
      method: 'post',
      body: JSON.stringify({
        title,
        // page: 0,
        // pageSize: 4,
      }),
      headers: { 'Content-Type': 'application/json' },
    })
      .then(res => res.json())
      .then(
        (searchReults) => {
          this.setState({
            searchReults,
          });
        },
        (error) => {
          // eslint-disable-next-line no-console
          console.error(error);
        },
      );
  };

  updateSeries = (series) => {
    fetch('/series', {
      method: 'put',
      body: JSON.stringify(series),
      headers: { 'Content-Type': 'application/json' },
      // eslint-disable-next-line no-console
    }).catch(err => console.error(err));
  };

  render() {
    const { searchReults } = this.state;

    let seriesTable;

    if (searchReults.length > 0) {
      seriesTable = <SeriesTable series={searchReults} handleFavorite={this.handleFavorite} />;
    }

    return (
      <Fragment>
        <Input searchHint="Manga Title" handleChange={this.handleChange} />
        {seriesTable}
      </Fragment>
    );
  }
}
