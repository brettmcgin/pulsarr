import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

import { Search } from '@material-ui/icons';

const styles = theme => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    marginLeft: 0,
    width: '100%',
    backgroundColor: 'white',
  },
  searchIcon: {
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    marginLeft: '10%',
  },
});

const SearchInput = ({ classes, searchHint, handleChange }) => (
  <div className={classes.search}>
    <div className={classes.searchIcon}>
      <Search />
    </div>
    <InputBase className={classes.input} placeholder={searchHint} onChange={handleChange} />
  </div>
);

SearchInput.propTypes = {
  classes: PropTypes.object.isRequired,
  searchHint: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
};

SearchInput.defaultProps = {
  searchHint: 'Search…',
};

export default withStyles(styles)(SearchInput);
