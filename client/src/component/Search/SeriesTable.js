import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Favorite, FavoriteBorder } from '@material-ui/icons';

const styles = theme => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    marginLeft: 0,
    width: '100%',
    backgroundColor: 'white',
  },
  searchIcon: {
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    marginLeft: '10%',
  },
});

const SeriesTable = ({ classes, series, handleFavorite }) => (
  <Paper className={classes.root}>
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell>Favorite</TableCell>
          <TableCell align="left">Title</TableCell>
          <TableCell align="right">On Going</TableCell>
          <TableCell align="right">Source</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {series.map(s => (
          <TableRow key={s.title}>
            <TableCell>
              <Button onClick={() => handleFavorite(s)}>
                {s.favorite ? <Favorite /> : <FavoriteBorder />}
              </Button>
            </TableCell>
            <TableCell component="th" scope="row">
              <Button style={{ textTransform: 'none' }}>{s.title}</Button>
            </TableCell>
            <TableCell>{s.completed ? 'Completed' : 'Still Going'}</TableCell>
            <TableCell>{s.sourceName}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Paper>
);

SeriesTable.propTypes = {
  classes: PropTypes.object.isRequired,
  series: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      href: PropTypes.string.isRequired,
      completed: PropTypes.bool,
      dateScraped: PropTypes.string,
      sourceName: PropTypes.string,
      favorite: PropTypes.bool.isRequired,
      following: PropTypes.bool.isRequired,
    }),
  ).isRequired,
  handleFavorite: PropTypes.func.isRequired,
};

export default withStyles(styles)(SeriesTable);
