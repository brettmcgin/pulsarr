import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Search from '../Search/Search';
import Favorites from '../Favorites/Favorites';
import Convert from '../Convert/Convert';
import Detail from '../Detail/Detail';

const series = {
  _id: '5cedb1e1f056075174de556b',
  favorite: false,
  following: false,
  chapters: [],
  title: "JoJo's Bizarre Adventure Part 2: Battle Tendency",
  href: 'https://www.mangareader.net/jojos-bizarre-adventure-part-2-battle-tendency',
  completed: true,
  source: 'https://www.mangareader.net',
  sourceName: 'Manga Reader',
  dateScraped: new Date('2019-05-28T22:10:41.341Z'),
  __v: 0,
  artist: 'ARAKI Hirohiko',
  author: 'ARAKI Hirohiko',
  dateReleased: new Date('1987-01-01T00:00:00.000Z'),
  description:
    'SERIES 2: "Battle Tendency" The second arc in Jojo\'s Bizarre Adventure, Battle Tendency follows Joseph Joestar, Jonathan\'s grandson, as he battles other vampires and eventually the Pillar Men. It turns out these Pillar Men are the makers of the stone masks (which create normal vampires), and are after the Red Stone of Asia in order to become the ultimate life forms. Luckily for Joseph, he has an affinity to use Ripple and trains under Lisa Lisa. They form a team with her other disciples and some unexpected allies to defeat the Pillar Men.',
};

const MainRoutes = props => (
  <Switch>
    <Route exact path="/" {...props} component={Search} />
    <Route path="/detail" {...props} component={() => <Detail series={series} />} />
    <Route path="/favorites" {...props} component={Favorites} />
    <Route path="/convert" {...props} component={Convert} />
  </Switch>
);

export default MainRoutes;
