import React, { Fragment } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import PropTypes from 'prop-types';
import JssProvider from 'react-jss/lib/JssProvider';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import { generateClassName, jss, theme } from '../../utils/theme/theme';
import Header from '../Header';
import Routes from './Routes';

const styles = {
  root: {
    position: 'absolute',
    height: '100vh',
    backgroundColor: '#272727',
    width: '100%',
  },
  content: {
    maxWidth: 1200,
  },
};

const App = ({ classes }) => (
  <Fragment>
    <CssBaseline />

    <JssProvider jss={jss} generateClassName={generateClassName}>
      <MuiThemeProvider theme={theme}>
        <Router>
          <Grid container direction="column" alignContent="center" className={classes.root}>
            <Grid item className={classes.content}>
              <Header />

              <Routes />
            </Grid>
          </Grid>
        </Router>
      </MuiThemeProvider>
    </JssProvider>
  </Fragment>
);

App.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(App);
