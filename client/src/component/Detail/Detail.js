import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Favorite, Bookmark, BookmarkBorder } from '@material-ui/icons';

const styles = theme => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    marginLeft: 0,
    width: '100%',
    backgroundColor: 'white',
  },
  searchIcon: {
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    marginLeft: '10%',
  },
});

const Detail = ({ classes, series }) => {
  const [open, setOpen] = useState(true);

  function handleClose() {
    setOpen(false);
  }
  // TODO: add favorite and following actions
  return (
    <Fragment>
      <Dialog
        fullWidth
        maxWidth="md"
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title">
          {series.title}
          {' '}
          <Favorite />
          <Bookmark />
          <BookmarkBorder />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <div>
              Status:
              {series.completed ? 'Completed' : 'On Going'}
            </div>
            <div className={classes.search}>
              Source:
              {series.sourceName}
            </div>
            <div>
              Source:
              {series.sourceName}
            </div>
            <div>
              Release Date:
              {series.dateReleased.getFullYear()}
            </div>
            <div>
              Author:
              {series.author}
            </div>
            <div>
              Artist:
              {series.artist}
            </div>
            <p>{series.description}</p>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

Detail.propTypes = {
  classes: PropTypes.object.isRequired,
  series: PropTypes.shape({
    title: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    completed: PropTypes.bool,
    source: PropTypes.string,
    sourceName: PropTypes.string.isRequired,
    favorite: PropTypes.bool.isRequired,
    following: PropTypes.bool.isRequired,
    dateReleased: PropTypes.instanceOf(Date),
    author: PropTypes.string,
    artist: PropTypes.string,
    description: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(Detail);
